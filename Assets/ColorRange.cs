using UnityEngine;

[System.Serializable]
public struct ColorRange
{
    public Color color;
    public float maxHeight;
}