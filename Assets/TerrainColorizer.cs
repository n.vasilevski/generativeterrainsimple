using UnityEngine;

[RequireComponent(typeof(Terrain))]
public class TerrainColorizer : MonoBehaviour
{
    [System.Serializable]
    public class ColorRange
    {
        public Color color;
        public float minHeight;
        public float maxHeight;
    }

    public ColorRange[] colorRanges;

    void Start()
    {
        Terrain terrain = GetComponent<Terrain>();

        float[,,] map = new float[terrain.terrainData.alphamapWidth, terrain.terrainData.alphamapHeight, colorRanges.Length];

        // Create a terrain layer for each color range
        TerrainLayer[] terrainLayers = new TerrainLayer[colorRanges.Length];
        for (int i = 0; i < colorRanges.Length; i++)
        {
            Texture2D texture = ColorToTexture.CreateColorTexture(colorRanges[i].color);
            terrainLayers[i] = new TerrainLayer();
            terrainLayers[i].diffuseTexture = texture;
        }

        terrain.terrainData.terrainLayers = terrainLayers;

        for (int y = 0; y < terrain.terrainData.alphamapHeight; y++)
        {
            for (int x = 0; x < terrain.terrainData.alphamapWidth; x++)
            {
                int heightmapX = (int)(x * terrain.terrainData.heightmapResolution / (float)terrain.terrainData.alphamapWidth);
                int heightmapY = (int)(y * terrain.terrainData.heightmapResolution / (float)terrain.terrainData.alphamapHeight);
                float height = terrain.terrainData.GetHeight(heightmapY, heightmapX);
                float normalizedHeight = height / terrain.terrainData.size.y;

                for (int i = 0; i < colorRanges.Length; i++)
                {
                    if (normalizedHeight >= colorRanges[i].minHeight && normalizedHeight < colorRanges[i].maxHeight)
                    {
                        map[x, y, i] = 1;
                    }
                    else
                    {
                        map[x, y, i] = 0;
                    }
                }
            }
        }

        terrain.terrainData.SetAlphamaps(0, 0, map);
    }
}
