using System;
using UnityEngine;

public class TerrainGenerator : MonoBehaviour
{
    public int width = 256;  // width of the terrain
    public int height = 256; // height of the terrain
    public int depth = 20; // maximum altitude of the terrain
    public float scale = 20.0f; // scale of the noise
    public float offsetX = 100f; // to offset the perlin noise function
    public float offsetY = 100f; // to offset the perlin noise function
    public AnimationCurve heightCurve; // animation curve for height modification

    private Terrain terrain;
    void Awake()
    {
         terrain = GetComponent<Terrain>();
        terrain.terrainData = GenerateTerrain(terrain.terrainData);
    }

    private void Update()
    {
        terrain.terrainData = GenerateTerrain(terrain.terrainData);

    }

    TerrainData GenerateTerrain(TerrainData terrainData)
    {
        terrainData.heightmapResolution = width + 1;

        terrainData.size = new Vector3(width, depth, height);

        terrainData.SetHeights(0, 0, GenerateHeights());

        return terrainData;
    }

    float[,] GenerateHeights()
    {
        float[,] heights = new float[width, height];
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                heights[x, y] = CalculateHeight(x, y);
            }
        }

        return heights;
    }

    float CalculateHeight(int x, int y)
    {
        float xCoord = (float)x / width * scale + offsetX;
        float yCoord = (float)y / height * scale + offsetY;

        // Use the animation curve to modify the output of the Perlin noise
        return heightCurve.Evaluate(Mathf.PerlinNoise(xCoord, yCoord));
    }
}