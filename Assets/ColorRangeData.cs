using UnityEngine;

[CreateAssetMenu(fileName = "ColorRangeData", menuName = "Terrain/Color Range Data")]
public class ColorRangeData : ScriptableObject
{
    [System.Serializable]
    public struct ColorRange
    {
        public float height;
        public Color color;
    }

    public ColorRange[] colorRanges;
}